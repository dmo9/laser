# The foundational codebase


## Introduction
The overall purpose of this project is to create a very strong foundation through a well-documented codebase. This organized collection of tools will allow the company to:

    * streamline day-to-day operations
    * increase customer service & satisfaction 
    * rapidly develop better products & services
    * capture the attention of more people
    * drastically reduce time to market of new features 
    * produce better research


Currently, this portion of the project contains code which:   

1. retreives & writes sales information to our database 
2. calculates and pays monthly commission to each of our artists
3. greatly simplifies the shipping process through a custom shipping client


## How to run the code:
The script files in the code folder are run locally in terminal. They call the modules from the modules folder


## what's next
* Code needs to be restructured and organized better. 
* Shipping client needs to be finished up 
* More comments added to the code 


## future additions:
* A django based ecommerce & content website
* scientific image analysis using sci-kit image
* generation of the daily laser engraving files (currently done with php & autoit)
* ensure all customer communication has been responded to (and promptly)


## Built With

* [SQLAlchemy](https://www.sqlalchemy.org/) - database ORM
* [Postgresql](https://www.postgresql.org/) - database
* [Paypal SDK](https://github.com/paypal/PayPal-Python-SDK) - for paying artist commissions
* [Shipengine](https://www.shipengine.com/) - the shipping client  