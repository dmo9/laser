from modules import db
from modules.db import Transaction, Order, Weight, AddOn, Product
import time


# used in the shipping section of the code 

def separate_sku(sku):
    '''
    Separates sku into its 4 components: category, artist_id, design_id, and add_on
    Returns a dict with these components as keys.
    '''

    category = sku[:3]
    artist_id = sku[4:-5]   #egr-0000-k1
    design_id = sku[4:-3] 
    add_on = sku[10:]

    data = {'category':category, 'artist_id':artist_id, 'design_id':design_id, 'add_on':add_on}

    return data

def get_skus(receipt_id):
    '''
    Returns a list of skus for a given receipt_id
    '''
    
    objects = [Transaction.sku]
    filter = Transaction.receipt_id == receipt_id

    response = db.query(objects,filter)
    count = response.count()
    data = response.all()

    skus = []
    for i in range(count):
        dude = data[i][0]
        skus.append(dude)

    return skus

def get_add_ons(receipt_id):
    '''
    Returns the add_on code for a given receipt_id
    '''
    
    skus = get_skus(receipt_id)
    
    add_ons = []
    for dude in skus:
        add_on = separate_sku(dude)['add_on']
        add_ons.append(add_on)

    return add_ons

def get_quantities(receipt_id):
    '''Returns a list of quantities for each transaction associated with a receipt_id'''
    add_ons = get_add_ons(receipt_id)

    objects = [Transaction.quantity]
    filter = Transaction.receipt_id == receipt_id
    response = db.query(objects,filter)
    count = response.count()
    data = response.all()

    quantities = []
    for i in range(count):
        quantity = data[i][0]
        quantities.append(quantity)
        
    # determine which categories the add_ons are from 
    for i in range(len(add_ons)):
        add_on = add_ons[i]
        if add_on != '0' and add_on != '1':                
            quantities.append(quantities[i])

    return quantities

def get_categories(receipt_id):
    ''' Returns a list of each product category purchased for a given receipt_id'''

    
    skus = get_skus(receipt_id)
    add_ons = get_add_ons(receipt_id)
    
    categories = []
    for dude in skus:
        category = separate_sku(dude)['category']
        categories.append(category)

    # determine which categories the add_ons are from 
    for i in range(len(add_ons)):
        
        if add_ons[i] != '1' and add_ons[i] != '0':

            objects = [AddOn.add_on_category]
            filter1 = AddOn.add_on == add_ons[i]
            filter2 = AddOn.category == categories[i]
            category = db.query(objects, filter1, filter2).all()[0][0]
            categories.append(category)

    

    return categories

def get_all_categories():
    ''' returns a list of all product categories in our store'''
    objects = [Product.category]

    response = db.query(objects)

    categories = []

    for i in range(response.count()):
        category = response.all()[i][0]
        categories.append(category)

    return categories

def determine_box(receipt_id):
    '''
    Based on the items in an order, determine which box should be chosen
    '''
    # get the data & 
    qty_per_cat = quantity_per_category(receipt_id)
    quantities = qty_per_cat['quantities']
    categories = qty_per_cat['categories']
    filters = []
    objects = [Weight.box]
    

    # create a filter where each category == the corresponding quantity
    for i in range(len(categories)):
        category = categories[i]
        attr = getattr(Weight, category)
        filter = attr == quantities[i]
        filters.append(filter)


    response = db.query(objects, *filters).all()
    if response:
        box = response[0][0]
    else:
        box = 0
    
    return box

def determine_total_weight(receipt_id):
    
    # get the data & 
    quantities = get_quantities(receipt_id)
    categories = get_categories(receipt_id)
    all_categories = get_all_categories()
    filters = []
    objects = [Weight.weight]
    
    # filter out product categories that do not appear for a given receipt_id
    a = set(categories)
    b = set(all_categories)
    non_categories = b-a
    
    if b-a:
        for category in non_categories:
            attr = getattr(Weight, category)
            filter = attr == 0
            filters.append(filter)

    # create a filter where each category == the corresponding quantity
    for i in range(len(categories)):
        category = categories[i]
        attr = getattr(Weight, category)
        filter = attr == quantities[i]
        filters.append(filter)


    response = db.query(objects, *filters).all()
    if response:
        weight = response[0][0]
    else:
        weight = 1.0

    
    return weight

def quantity_per_category(receipt_id):
    
    quantities = get_quantities(receipt_id)
    categories = get_categories(receipt_id)
    all_categories = get_all_categories()

    a = set(categories)
    b = set(all_categories)
    non_categories = b-a

    # if the user didn't purchase an item from every category
    if b-a:
        for category in non_categories:
            quantities.append(0)
            categories.append(category)

    data = {'quantities':quantities, 'categories':categories}
    return data




