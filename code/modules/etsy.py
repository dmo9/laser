import os
import pprint
import math
from sqlalchemy import create_engine, exc
from sqlalchemy.orm import sessionmaker
from requests_oauthlib import OAuth1Session
from modules import db
from modules.db import Order, Transaction, Base



  

def make_api_request(url, params):
    '''
    a request is made to an API and the response's json data is returned. 
    Url: the base url to make the request to
    params: any parameters to add on to the base url
    
    '''

    # keys acquired through the oauth process
    api_key = os.environ['ETSY_API_KEY']
    api_secret = os.environ['ETSY_API_SECRET']
    oauth_key = os.environ['ETSY_OAUTH_KEY']
    oauth_secret = os.environ['ETSY_OAUTH_SECRET']  


    # get the json data from the oauth protected url      
    oauth = OAuth1Session(api_key, client_secret = api_secret, resource_owner_key = oauth_key, resource_owner_secret = oauth_secret)
    r = oauth.get(url, params=params).json()
    return r

def terminal_clean_print(data):
    """prints clean output to the terminal via pprint"""
    
    
    os.system("clear") # clear the terminal
    print()
    print()
    pprint.pprint(data)
    print()
    print()
    print()
    print()

def start_db_session():
    engine = create_engine("postgresql://dave:password@localhost/etsydb")
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()

    return session

def get_receipt_data(was_shipped = 0, limit = 1, offset = 0, was_paid = 1):
    
    '''
    This function makes an oauth authenticated request to etsy's receipt endpoint. The function 
    returns all json data from that end point. 
    Api reference: https://www.etsy.com/developers/documentation/reference/receipt  '''


    url = 'https://openapi.etsy.com/v2/shops/topboro/receipts'
    params = {"was_shipped":was_shipped, "offset":offset,"limit":limit,"was_paid":was_paid}
    data = make_api_request(url,params)

    return data

def get_transaction_data(receipt_id):
    '''
    This function makes an oauth authenticated request to etsy's transaction endpoint. The function 
    returns all json data from that end point. 
    Api reference: https://www.etsy.com/developers/documentation/reference/transaction'''

    url = 'https://openapi.etsy.com/v2/receipts/' + str(receipt_id) + '/transactions'
    params      = {"offset":0}
    data = make_api_request(url,params)
    
    return data

def write_transaction_data(response):

    # start a db session 
    session = start_db_session()
    
    # determine the number of transactions for a given receipt id 
    number_of_transactions = response['count']
    results = response['results'] 

    # get & write transaction data for each transaction
    for i in range(number_of_transactions):

        if results[i]['product_data']: # receipt_id 1192216273 and older cancellations have product data:None
            sku = results[i]['product_data']['sku']
        else:
            sku = '0'

        # make a new instance for the Transaction class 
        new_transaction = Transaction(
            sku = sku,
            transaction_id = results[i]['transaction_id'],
            listing_id = results[i]['listing_id'],
            quantity = results[i]['quantity'],
            receipt_id = results[i]['receipt_id'],
            price = results[i]['price'],
            url = results[i]['url']
        )

        # Insert a new Transaction into the transactions table
        try:
            session.add(new_transaction)
            session.commit()
        except exc.IntegrityError as e:
            session.rollback()
            pass
        

    return None

def write_receipt_data(response):
    '''
    Takes in raw json data and only writes the values which we are interested in to the  db.
    Returns an array which contains all receipt_ids
    
    '''

    # start a db session 
    session = start_db_session()

    # Filter json & Initialize empty arrays 
    results = response['results'] #all values are within the 'results' array of etsy json response
    receipt_ids = []    

    # Instantiate each order object. An etsy api call can have multiple orders so we loop through each
    for i in range(len(results)):

        receipt = results[i]['receipt_id'] # was made a variable bc the function returns these

        new_order = Order(
            receipt_id = receipt,
            was_paid = results[i]['was_paid'],
            creation_tsz = results[i]['creation_tsz'],
            total_shipping_cost = results[i]['total_shipping_cost'],
            total_tax_cost =  results[i]['total_tax_cost'],
            discount_amt = results[i]['discount_amt'],
            adjusted_grandtotal = results[i]['adjusted_grandtotal'],
            subtotal = results[i]['subtotal'],
            gift_message = results[i]['gift_message'],
            is_gift = results[i]['is_gift'],
            message_from_buyer = results[i]['message_from_buyer'],
            buyer_user_id = results[i]['buyer_user_id'],
            buyer_email = results[i]['buyer_email'],
            was_shipped = results[i]['was_shipped'],
            name = results[i]['name'],
            first_line = results[i]['first_line'],
            second_line = results[i]['second_line'],
            city = results[i]['city'],
            state = results[i]['state'],
            zip = results[i]['zip'],
            country_id = results[i]['country_id']
        )

        # create an array with all receipt_ids. For use in th transaction api calls
        receipt_ids += [receipt]

        # Insert a new Order into the orders table
        try:
            session.add(new_order)
            session.commit()
        except exc.IntegrityError as e:
            session.rollback()
            pass


    return receipt_ids

def minimum_request_amount(max_limit, was_shipped):


    response = get_receipt_data(was_shipped = was_shipped, limit = 1, offset = 0, was_paid = 1)

    # the number of results for a given was_shipped status
    count = response['count']
    iterations = math.ceil(count/max_limit)


    return iterations

