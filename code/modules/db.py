''' 
IMPORT LIBRARIES
'''
import time
from datetime import datetime
from sqlalchemy import create_engine, UniqueConstraint, func, Column, ForeignKey, Integer, String, Boolean, Float, Text, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Query, relationship
from sqlalchemy.pool import NullPool




''' 
DATABASE FUNCTIONS
'''
def engine_constructor():
    ''' this should be in a different module / database'''
    
    db_user = 'dave'
    db_pass = 'password'
    db_name = 'etsydb'
    db_type = 'postgresql'
    db_host =  'localhost'

    engine = create_engine("postgresql://dave:password@localhost/etsydb", echo = False, poolclass=NullPool)


    return engine

def start_db_session():

    engine = engine_constructor()

    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()

    return session

def query(objects, *args, **kwargs):
    '''
    a general purpose db query function
    '''
    filters = []

    # append all additional filters input as args.
    for arg_filter in args:
        filters.append(arg_filter)


    # create & append additional filters for values input as kwargs. Ex: artist_id = '01' 
    for key, value in kwargs.items():
        filter = kwarg_filter(key,value)
        filters.append(filter)


    # start the databsase session, instantiate a Query, filter the Query
    session = start_db_session()
    response = session.query(*objects)
    for i in filters:
        response = response.filter(i)
    
    # end the session
    session.commit()
    session.close()

    return response

def insert(object, data):
    '''
    Inserts a single row into the db. 
    object: an instantiated object that represents a table
    data: a dictionary with keys corresponding to column names and values to values
    
    '''


    # start the database session
    session = start_db_session()    

    # fucking magic ** operator unpacks the dictionary 
    instance = object(**data)
    session.add(instance)
    session.commit()
    session.close()

    return None 

def update(object, updates, *args):
    '''
    Update a single row in the db.based  
    table_class: an instantiated object that represents a table
    updates: a dictionary where set(key) = value
    
    '''

    # example of a complete query: session.query(FoobarModel).filter(FoobarModel.id == foobar_id).update({'name': 'New Foobar Name!'})


    filters = []

    # append all filters input as args.
    for arg_filter in args:
        filters.append(arg_filter)

    # start the database session & make a query
    session = start_db_session()    
    response = session.query(object)
    
    # apply the where filters
    for i in filters:
        response = response.filter(i)


    # apply the set value updates
    response.update(updates)
    session.commit()


    return response

def start_filter(start_date):
    start_epoch = date_to_epoch(start_date,'start')
    start_filter = Order.creation_tsz >= start_epoch

    return start_filter

def end_filter(end_date):
    
    # convert the dates to epoch
    end_epoch = date_to_epoch(end_date,'end')
   
    # define the filters
    end_filter = Order.creation_tsz <= end_epoch
    
    return end_filter

def category_filter(category):

    category_string = category + '%' # i.e 'EDU%'
    filter = Transaction.sku.like(category_string)
    return filter

def design_filter(design_id):

    design_id_string = '____' + design_id + '%' # i.e '____01%'
    filter = Transaction.sku.like(design_id_string)
    return filter

def artist_filter(artist_id):
   
    artist_id_string = '____' + artist_id + '%' # i.e '____01%'
    filter = Transaction.sku.like(artist_id_string)
    return filter

def kwarg_filter(key, value):

    
    if key == 'artist_id':
        filter = artist_filter(value)

    elif key == 'category':
        filter = category_filter(value)

    elif key == 'design_id':
        filter = design_filter(value)

    return filter

def date_to_epoch(date, type):

    # break the date into its components
    year = date[0]
    month = date[1] 
    day = date[2] 

    # if it is an end date, we want the last second of the day. Otherwise, start on second zero
    if type == 'end':
        date_time = datetime(year, month, day, 23, 59, 59)  
    elif type =='start':
        date_time = datetime(year, month, day)
    
 
    # do some time conversion magic
    epoch = time.mktime(date_time.timetuple())
      
    return epoch




''' 
DATABASE TABLES
'''
Base = declarative_base()


class Order(Base):
    """"""
    __tablename__ = 'orders'
    
    receipt_id = Column(Integer, primary_key=True, nullable=False)
    buyer_user_id = Column(Integer)
    creation_tsz = Column(Integer)


    was_paid = Column(Boolean)
    was_engraved = Column(Boolean, default = False) # indicates if the purcahses for this order have been engraved
    was_shipped = Column(Boolean) # indicates if a shipping label has been printed for this order
    is_gift = Column(Boolean)

    gift_message = Column(Text)
    message_from_buyer = Column(Text)
    buyer_email = Column(String)
    name = Column(String, nullable=False)
    first_line = Column(String, nullable=False)
    second_line = Column(String)
    city = Column(String, nullable=False)
    state = Column(String)
    zip = Column(String, nullable=False)
    country_id = Column(Integer, nullable=False) # a 3 digit country code. Must lookup in the country table to get the country name


    total_shipping_cost = Column(Float) # total amount of postage, if any, the buyer paid
    total_tax_cost = Column(Float) # total amount of tax the buyer paid
    discount_amt = Column(Float)  # monetary amount of any discounts
    subtotal = Column(Float) # revenue of the sale. Includes discounts, excludes tax & shipping.
    adjusted_grandtotal = Column(Float) # total amount of money brought in, including everything

    # Create a one-to-many relationship between orders and transactions
    transactions = relationship("Transaction")

 
class Transaction(Base):
    __tablename__ = 'transactions'
    
    transaction_id = Column(Integer, primary_key=True)
    listing_id = Column(Integer)
    receipt_id = Column(Integer, ForeignKey('orders.receipt_id'))

    sku = Column(String)
    quantity = Column(Integer)   
    price = Column(Float)
    url = Column(String)


class Design(Base):
    ''' Will be used for the engrave sheets'''
    __tablename__ = 'designs'
    
    design_id = Column(String, primary_key=True, nullable=False)
    design_name = Column(String, nullable = False) 


class Country(Base):
    '''contains the iso country code and the corresponding normal english  country name'''
    __tablename__ = 'countries'
    country_id = Column(Integer, primary_key=True)
    iso_country_code = Column(String, default = 0, nullable=False)


class Artist(Base):
    '''contains any information pertaining to the artists'''
    __tablename__ = 'artists'
    artist_id = Column(String, primary_key = True)
    name = Column(String)
    email = Column(String)
    ig = Column(String)
    is_commission = Column(Boolean, nullable = False) 
    transactions = relationship("Commission")


class Product(Base):
    __tablename__ = 'products'
    category = Column(String, primary_key = True)
    name = Column(String)
    length = Column(Float)
    width = Column(Float)
    height = Column(Float)
    weight = Column(Float, nullable = False)

    UniqueConstraint(name)


class Weight(Base):
    __tablename__ = 'weights'
    EDU = Column(Integer, primary_key = True)
    EGR = Column(Integer, primary_key = True)
    EJA = Column(Integer, primary_key = True)
    ENI = Column(Integer, primary_key = True)
    weight = Column(Float, default = 0)
    box = Column(String)
    

class Commission(Base):
    '''
    one row is created for each payout item sent to paypal
    '''

    __tablename__ = 'commissions'
    artist_id = Column(String, ForeignKey('artists.artist_id'), primary_key = True)
    month = Column(Integer, primary_key = True)
    year = Column(Integer, primary_key = True)
    commission = Column(Float)
    fee = Column(Float)
    status = Column(String, default = None)
    payout_item_id = Column(String)
    payout_batch_id = Column(String)

    # ensures there is only one month & year entry for each artist id 
    UniqueConstraint(artist_id, month, year)
   

class Shipment(Base):
    '''
    As shipengine.py creates a new shipment, it adds the open addresses to this table.
    When a shipment is shipped, tracking numbers and shipment id's are added to  
    '''

    __tablename__ = 'shipments'
    receipt_id = Column(Integer, primary_key = True)
    weight = Column(Float)
    box = Column(String)
    service_code = Column(String)
    carrier_id = Column(String)
    shipment_id = Column(String)
    tracking = Column(String)
    postage_total = Column(Float)
    external_batch_id =  Column(String)
    

class Warehouse(Base):
    __tablename__ = 'warehouses'
    name = Column(String)
    warehouse_id = Column(String, primary_key = True)
    origin_address = Column(JSON)
    return_address = Column(JSON)


class AddOn(Base):
    __tablename__ = 'add_ons'
    category = Column(String, primary_key = True)
    add_on = Column(String, primary_key = True)
    add_on_category = Column(String, ForeignKey('products.category'))

    # ensure an add_on key is unique for each category 
    UniqueConstraint(category, add_on)


#url structure:  dialect[+driver]://user:password@host/dbname[?key=value..]
engine = engine_constructor()


# create tables
Base.metadata.create_all(engine)


