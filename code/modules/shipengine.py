import requests
import os
from modules import db, dude
from modules.db import Order, Country, Shipment, Warehouse, Transaction



def get_receipt_ids():

     # get the raw open order data from the db
    filter1 = Order.was_shipped == False 
    objects = [Order.receipt_id]
    response = db.query(objects, filter1)

    receipt_ids = []
    for i in range(response.count()):
        receipt_id = response[i][0]
        receipt_ids.append(receipt_id)

    return receipt_ids

def get_open_addresses():
    '''
    Makes a request to the db for all addresses of orders which has not been shipped. Each address is put into the shpengine format,
    and stored in a dict. Each dict is stored in a list named addresses.
    '''

    # get the raw open order data from the db
    filter1 = Order.was_shipped == False 
    filter2 = Order.country_id == Country.country_id
    objects = [Order.name, Order.first_line, Order.second_line, Order.city, Order.state, Order.zip, Country.iso_country_code]
    response = db.query(objects, filter1, filter2)

    # store each address in a dict and all dicts in a list named addresses
    addresses = []
    for i in range(response.count()):

        address = {
            'name':response[i][0], 
            'address_line1':response[i][1], 
            'address_line2':response[i][2], 
            'city_locality':response[i][3], 
            'state_province':response[i][4], 
            'postal_code':response[i][5],
            'country_code':response[i][6]
        }

        addresses.append(address)

    return addresses

def create_default_shipments(addresses, receipt_ids):
    '''
    Create a default shipment for each open address. Defaults are determined by each in the default function series.
    
    addresses: a list of open addresses in shipengines format
    receipt_ids: a list of receipt_ids for the open orders

    '''

    # create a dictionary, shipments, which contains each shipment
    shipments = []
    for i in range(len(addresses)):
        
        address = addresses[i]
        receipt_id = receipt_ids[i]
        country_code = address['country_code']
        carrier = 'USPS'

        # get the defaults
        warehouse_id = default_warehouse()
        weight = dude.determine_total_weight(receipt_id)
        box = dude.determine_box(receipt_id)
        carrier_id = default_carrier_id(carrier)
        service_code = default_service_code(carrier, weight, country_code)
        packages = default_package(weight)

        # add the shipment to the database
        data = {'receipt_id':receipt_id, 'service_code':service_code, 'carrier_id': carrier_id, 'weight':weight, 'box':box }
        db.insert(Shipment, data)
        
        # create each shipment and add each one to the shipments list
        shipment = {
            'service_code': service_code,
            'carrier_id': carrier_id,
            'ship_to': address,
            'warehouse_id': warehouse_id,
            'packages': packages
        }

        shipments.append(shipment)


    # Send the shipments to shipengine
    shipments = {'shipments':shipments}
    return shipments

def send_shipments(shipments):

    '''
    Sends the shipments to shipengine and returns shipengine's shipment_ids for 
    each shipment. 

    shipments: a formatted dictionary containing each shipment to be made
    '''

    # send the shipments to shipengine
    api_key = os.environ['SHIPENGINE_KEY']
    url = 'https://api.shipengine.com/v1/shipments'
    headers = {'api-key':api_key, 'content-type':'application/json'}
    response = requests.post(url, json = shipments, headers = headers).json()

    # filter the response so we only return shipment_ids
    shipments = response['shipments']
    shipment_ids = []
    for i in range(len(shipments)):
   
        shipment_id = shipments[i]['shipment_id']
        shipment_ids.append(shipment_id)

    return shipment_ids

def add_shipment_ids_to_db(shipment_ids, receipt_ids):
    '''
    Add the shipment_id to the corresponding receipt_id in the shipments table using db.update 
    '''

    for i in range(len(shipment_ids)):
   
        shipment_id = shipment_ids[i]
        
        filter = Shipment.receipt_id == receipt_ids[i]
        updates = {'shipment_id':shipment_id}
        db.update(Shipment,updates, filter)
    
    return None

def default_warehouse():
    warehouse_id = 'se-291188'
    return warehouse_id

def default_package(total_weight):
    ''' 
    Puts the package information into format specified by shipengine.
    If dimensions need to be specified in the future, they can be added here

    total_weight: the total weight of the package including the products
    '''

    packages = []
    package = {
        'weight':{
            'value': total_weight,
            'unit':'ounce'
        }
    }

    # it's appended because shipengine is expecting an array of packages for some reason
    packages.append(package)
    return packages

def default_service_code(carrier, weight, country_code):

    '''
    a custom but limited function that applies to my company only. Based on the weight and country,
    one of three shipping services are selected.
    '''

    if carrier == 'USPS':
        # if the country is the united states
        if country_code == 'US':
            if weight <= 15.99:
                service_code = 'usps_first_class_mail'
                
            else:
                service_code = 'usps_priority_mail'


        # if the country is not in the us:
        else:
            service_code = 'usps_first_class_mail_international'


    return service_code 

def default_carrier_id(carrier):
    if carrier == 'USPS':
        carrier_id = 'se-146043'
        return carrier_id
    else:
        pass

def create_warehouse(name, origin_address, return_address):
    '''
    creates a warehouse object. Used in all batch shipments

    origin_address: the location from where the packages are departing
    return_address: where returns for this warehouse are sent to (often the same as origin)  
    name: an internal name for this warehouse

    '''

    # data to be sent to the shipengine api
    warehouse = {
        'name':name,
        'origin_address': origin_address,
        'return_address': return_address
    }

    # make a request with our api key
    api_key = os.environ['SHIPENGINE_KEY']
    url = 'https://api.shipengine.com/v1/warehouses'
    headers = {'api-key':api_key, 'content-type':'application/json'}
    response = requests.post(url, json=warehouse, headers = headers).json()
    
    # return the newly created warehouse id
    warehouse_id = response['warehouse_id']


    return warehouse_id

def add_warehouse_to_db(warehouse_id, name, origin_address, return_address):
    '''
    inserts a new warehouse into the warehouse table 

    warehouse_id (string): shipengine's internal id for this warehouse
    name (string): our internal name for this warehouse
    origin_address (json): the location from where the packages are departing
    return_address (json): where returns for this warehouse are sent to (often the same as origin)  

    '''

    data = {
        'warehouse_id':warehouse_id,
        'name':name, 
        'origin_address':origin_address, 
        'return_address':return_address
    }
    
    db.insert(Warehouse, data)

def update_shipment(shipment_id, updates):
    
    api_key = os.environ['SHIPENGINE_KEY']
    url = 'https://api.shipengine.com/v1/shipments/' + shipment_id
    headers = {'api-key':api_key, 'content-type':'application/json'}
    response = requests.put(url, json = updates, headers = headers)
    return response.json()

def query_shipment(shipment_id):

    '''
    Gets information for a shipment from the shipment_id
    '''


    api_key = os.environ['SHIPENGINE_KEY']
    url = 'https://api.shipengine.com/v1/shipments/' + shipment_id
    headers = {'api-key':api_key, 'content-type':'application/json'}
    response = requests.get(url,headers = headers).json()

    return response

def create_batch(shipment_ids):
    external_batch_id = '18-03-12'

    data = {
        "external_batch_id": external_batch_id,
        "shipment_ids": shipment_ids
    }

    api_key = os.environ['SHIPENGINE_KEY']
    headers = {'api-key':api_key, 'content-type':'application/json'}
    url = ' https://api.shipengine.com/v1/batches'
    response = requests.post(url, headers = headers, json = data).json()
    batch_id = response[0]['batch_id']
    label_download = response[0]['label_download']

    data = {'batch_id':batch_id, 'label_download':label_download}


    return data

def process_batch(batch_id):
    url =  'https://api.shipengine.com/v1/batches/' + batch_id + '/process/labels'
    data = {
        "label_layout": "4x6",
        "label_format": "zpl",
        "test_label": True
    }
    api_key = os.environ['SHIPENGINE_KEY']
    headers = {'api-key':api_key, 'content-type':'application/json'}
    requests.post(url, headers = headers, json = data)

    return None

def create_webhook(webhook_url):


    url = 'https://api.shipengine.com/v1/environment/webhooks'
    webhook_url = 'www.topboro.com/ooooyeahhh'
    data = {'url':webhook_url, 'event':'batch'}
    api_key = os.environ['SHIPENGINE_KEY']
    headers = {'api-key':api_key, 'content-type':'application/json'}

    requests.post(url, headers = headers, json = data) # doesn't return anything

    return None