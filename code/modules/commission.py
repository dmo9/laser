''' 
IMPORT LIBRARIES
'''

import pprint
import os
import collections
from sqlalchemy import create_engine, func
from modules import db
from modules.db import Artist, Order, Transaction, Commission


''' 
DEFINE FUNCTIONS
'''

def separate_data_by_object(objects, response):
    '''
    Separates the raw response from db.query function into arrays for each object.
    Ex. a raw transaction might be split into skus, quantities, receipt_ids   

    '''
    
    
    # counts & array initialization
    num_objects = len(objects)
    data = response.all() # all rows from the response
    row_count = response.count() # number of rows in the response
    data_by_object = []

    # for each object, add a dimension to data_by_object & add corresponding column data for every row
    for i in range(num_objects):
        data_by_object.append([])

        # add each column from all rows to the corresponding location in data_by_object
        for j in range(row_count):
            data_by_object[i].append(data[j][i])

    return data_by_object
 
def separate_skus(skus, print_output = 0):
    '''
    Take an input of skus and separates it into categories, artist_id, design_id, and add_ons 
    '''

    #initialize empty arrays
    categories = [] # the quantity for
    artist_ids = []
    design_ids = [] # an identifier that is the primary key for Order and foreign key for Transaction.
    add_ons = []
    
    # A for loop is necessary because we must iterate through each table row returned by the query
    for sku in skus:
        
        # sku breakdown: 
        category = sku[:3]
        artist_id = sku[4:-5]   #egr-0000-k1
        design_id = sku[4:-3] 
        add_on = sku[10:]

        # write data to the relational arrays. Array length = number of rows from query
        categories += [category] 
        artist_ids += [artist_id]
        design_ids += [design_id]
        add_ons += [add_on]
        
        if print_output == 1:
            print(category, artist_id, design_id, add_on)
        else:
            pass

    return [categories, artist_ids, design_ids, add_ons]

def get_unique_values(list_param):
    ''' Takes in a list and returns a list of all unique entries in that list'''

    # creates a dictionary with unique entires as keys and the count of each key as values  
    counter = collections.Counter(list_param)
    keys = counter.keys() # only interested in keys since the values aren't accurate due to quantities
    unique_list = list(keys)

    return unique_list

def get_separated_sku(start_date, end_date, unique = 1):
    
    # define time filters
    start_filter = db.start_filter(start_date)
    end_filter = db.end_filter(end_date)

    # define the objects &  table relations
    objects = [Transaction.sku, Transaction.quantity, Transaction.receipt_id, Transaction.price]
    relation = Order.receipt_id == Transaction.receipt_id
    
    # get the data
    response = db.query(objects, start_filter, end_filter, relation)

    # separate the data and get the skus
    separated_data = separate_data_by_object(objects, response)
    skus = separated_data[0]

    #separate the skus into its components: categories, artist_ids, design ids, and add ons 
    separated_skus = separate_skus(skus)
    categories = separated_skus[0] # a list of the category from each row
    artists = separated_skus[1]
    design_ids = separated_skus[2]
    add_ons = separated_skus[3]

    # either return all values or just return unique values

    if unique == 1:
        categories = get_unique_values(categories)
        artist_ids = get_unique_values(artists)
        design_ids = get_unique_values(design_ids)
        add_ons = get_unique_values(add_ons)
    else:
        pass
    
    # put the data in a nice dict so we can retrieve it by key
    all_data = {'categories': categories, 'artist_ids':artist_ids, 'design_ids': design_ids, 'add_ons': add_ons}


    return all_data

def quantity_per_category(start_date, end_date, category, artist_id = None):

    # create the where filters and define the quantity-sum select object
    start_filter = db.start_filter(start_date)
    end_filter = db.end_filter(end_date)
    relation_filter = Order.receipt_id == Transaction.receipt_id
    objects = [func.sum(Transaction.quantity)]

    # if passed an artist_id, only get the quantity for that artist. Otherwise, get sum of quantity for all artists
    if artist_id:
        response = db.query(objects, start_filter, end_filter, relation_filter, artist_id = artist_id, category = category).all()

    else:
        response = db.query(objects, start_filter, end_filter, relation_filter, category = category).all()


    quantity = response[0][0]

    if quantity == None:
        quantity = 0
    else:
        pass

    return quantity

def revenue_per_category(start_date, end_date, category, artist_id = None):

    # create the where filters and define the quantity-sum select object
    start_filter = db.start_filter(start_date)
    end_filter = db.end_filter(end_date)
    relation_filter = Order.receipt_id == Transaction.receipt_id
    objects = [Transaction.price, Transaction.quantity, Order.discount_amt]



    # if passed an artist_id, only get the quantity for that artist. Otherwise, get sum of quantity for all artists
    if artist_id:
        response = db.query(objects, start_filter, end_filter, relation_filter, artist_id = artist_id, category = category).all()

    else:
        response = db.query(objects, start_filter, end_filter, relation_filter, category = category).all()

    # revenue if the price multiplied by quantity ordered minus the discount amount for the sale
    revenue = 0
    if response:
        for i in range(len(response)):
            price = response[i][0]
            quantity = response[i][1]
            discount = response[i][2]
            revenue += (price*quantity-discount)
    else:
        pass


    return revenue

def commission_per_artist(start_date, end_date, category, artist_id):
    

    revenue = revenue_per_category(start_date, end_date, category, artist_id)
    commission = revenue*0.05

    return commission

def ignore_noncommission_artists(artist_ids):
    '''
    Takes in a list of artists and returns only artists who are paid commission
    '''

    objects = [Artist.is_commission]
    artists = []

    for artist_id in artist_ids:
        filter1 = Artist.artist_id == artist_id
        response = db.query(objects, filter1).all()

        # If the artist is commissioned, then add them to the list
        if response[0][0] == True:
            artists.append(artist_id)
            

    return artists
      
def calculate_commission(month, year, start_date, end_date):

    # get unique categories & artists
    unique = get_separated_sku(start_date, end_date, unique = 1)
    categories = unique['categories']
    artist_ids = unique['artist_ids']

    # filter artists so that we only have commissioned ones
    artist_ids = ignore_noncommission_artists(artist_ids)

    # write sales & commission information for each artist to commissions table 
    for artist_id in artist_ids:
        
        commission = 0
        for category in categories:
            commission += commission_per_artist(start_date, end_date, category, artist_id)

        data = {'commission':commission, 'month': month, 'year':year, 'artist_id': artist_id} 
        db.insert(Commission, data)

def sales_and_commission(start_date, end_date, unique_categories, unique_artists):
    '''

    Creates an entry in the commissions table for each artist that has made a sale during given time period.
    Calculates the total_commission, total_revenue, and number of sales per category for each artist

    Inputs:
    start_date: The beginning of the time period in [yyyy,mm,dd] format
    end_date: end of the time period in the same format
    unique_categories: a list of all the categories that had sales for the time period
    unique_artists: list of all artist_ids that has sales for the given time period
   
    '''
    
    for artist_id in unique_artists:

        # initialize sum variables
        month = start_date[1]
        year = start_date[0]
        artist_commission = 0
        artist_revenue = 0.000
        artist_sales = 0
        object = Commission


        # create a new row  in the commissions table for each artist that made a sale for the given month
        insert = {'artist_id':artist_id, 'month':month, 'year':year}
        db.insert(object, insert)
   
        # get the revenue & quantity sold for each category
        for category in unique_categories:
            
            # get the quantity and revenue for a given artist id and category
            quantity = quantity_per_category(start_date, end_date, category, artist_id)
            revenue = revenue_per_category(start_date, end_date, category, artist_id)
            print(revenue)
            

            # add quantity sold per category to commissions table for the given category, date, and artist_id
            updates = {category:quantity}
            db.update(object, updates, object.year == year, object.month == month, object.artist_id == artist_id)
            
            # create sums n shit
            commission = revenue*0.05
            artist_commission +=commission
            artist_revenue += revenue

        # add total revenue and total commission for each artist of a given time period
        updates = {'total_revenue':artist_revenue, 'total_commission':artist_commission}
        db.update(Commission, updates, object.year == year, object.month == month, object.artist_id == artist_id)





    













