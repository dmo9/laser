# the paypal payouts api: https://developer.paypal.com/docs/integration/direct/payouts/integration-guide/payouts_api/
# SDK reference: https://github.com/paypal/PayPal-Python-SDK#payouts


''' 
IMPORT LIBRARIES
'''
import os
import random
import string
from paypalrestsdk import Payout, PayoutItem, ResourceNotFound, configure
from modules import db
from modules.db import Commission, Artist


''' 
DEFINE FUNCTIONS
'''

def create_payout_item(artist_id, commission, month, year):


    objects = [Artist.name, Artist.is_commission, Artist.email]
    filter = Artist.artist_id == artist_id
    response = db.query(objects, filter).all()


    artist_email = response[0][2]
    artist_is_commission = response[0][1]

    if artist_is_commission:

        item = {
            'receiver': artist_email,
            'note':'topboro commission for ' + month + '/' + year,
            'sender_item_id':'S'+ artist_id + month + year,
            'amount':{
                'value': round(commission,2), 
                'currency': "USD"
            }
        }
        return item

def authenticate():
    '''
    Used to authenticate our request to the paypal api
    '''

    mode = os.environ['PAYPAL_MODE']
    client_id = os.environ['PAYPAL_CLIENT_ID']
    client_secret = os.environ['PAYPAL_CLIENT_SECRET']

    
    configure({
        "mode": mode, 
        "client_id":  client_id,
        "client_secret": client_secret
    })

    return None

def pay(items, month, year):
   
    '''
    There are two batch ids: the one we specified, and the one paypal uses for internal purposes
    
    '''
    
    authenticate()


    sender_batch_id = month + year + ''.join(random.choice(string.ascii_uppercase) for i in range(12))
    email_subject = 'your Topboro commission has been posted'


    payout = Payout({
        "sender_batch_header": {
            'sender_batch_id': sender_batch_id,
            'email_subject': email_subject,
            'recipient_type':'EMAIL',
        },
        "items": items
    })

    if payout.create(sync_mode=False):
        payout_batch_id = payout.batch_header.payout_batch_id
        print("payout[%s] created successfully" %(payout_batch_id))
        return payout_batch_id, sender_batch_id
    else:
        print(payout.error)

def get_batch(payout_batch_id):

    authenticate()


    try:
        payout = Payout.find(payout_batch_id)
        return payout

    except ResourceNotFound as error:
        print("Web Profile Not Found")

def get_item(payout_item_id):
   
    authenticate()
   
    try:
        payout_item = PayoutItem.find(payout_item_id)
        return payout_item

    except ResourceNotFound as error:
        print("Payout Item Not Found")

def add_payouts_to_db(month, year, payout_batch_id):
    
    # get the data for each payout
    response = get_batch(payout_batch_id)
    items = response['items']

    # for each payout
    for i in range(len(items)):


        # get the artist id from the sender_item_id
        sender_item_id = items[i]['payout_item']['sender_item_id']
        artist_id = sender_item_id[1:3]

        # get data from certain fields
        transaction_status = items[i]['transaction_status']
        commission = items[i]['payout_item']['amount']['value']
        fee = items[i]['payout_item_fee']['value']
        payout_item_id = items[i]['payout_item_id']

        # store the data in a nice dict for db.insert
        updates = {
            'payout_item_id': payout_item_id,
            'status': transaction_status,
            'fee': fee,
            'payout_batch_id': payout_batch_id,
           
        }

        filter1 = Commission.artist_id == artist_id
        filter2 = Commission.month == month
        filter3 = Commission.year == year
    
        # insert the data into the payouts table
        db.update(Commission, updates, filter1, filter2, filter3)

def create_webhook(url):
    
    webhook = Webhook({
    "url": url,
    "event_types": [
        {
            "name": "PAYMENT.AUTHORIZATION.CREATED"
        },
        {
            "name": "PAYMENT.AUTHORIZATION.VOIDED"
        }
    ]
    })

    if webhook.create():
        print("Webhook[%s] created successfully" % (webhook.id))
    else:
        print(webhook.error)

def replace_webhook(webhook_id):
    return None