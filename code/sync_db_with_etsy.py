# This script uses the etsy module to retreive data from their api and write it to our database 

'''
IMPORT MODULES
'''
from modules import etsy


'''
RUN SCRIPT
'''
# set the results per request with max_limit & initialize variables
max_limit = 100
offset = 0
was_shipped = 1

# minimize the number of requests sent to the api by maxing results per request. less requests = faster
iterations = etsy.minimum_request_amount(max_limit, was_shipped)

# TODO Might be really slow and need to be redone 
# for each request, extract all receipt data, write it to the db, and return an array of receipt_ids 
for i in range(iterations):

    # make a request to to the etsy api
    response = etsy.get_receipt_data(was_shipped = was_shipped, limit = max_limit, offset = offset, was_paid = 1)
    offset += max_limit #increase offset for the next iteration of receipt data
    
    # get the receipt ids from the response. To be used for transaction requests
    receipt_ids = etsy.write_receipt_data(response)

    # for each receipt_id, get the transactions and write them to the database 
    for receipt_id in receipt_ids:
        response = etsy.get_transaction_data(receipt_id)
        etsy.write_transaction_data(response)


