from modules import db, commission

# time period
start = [2018,2,28]
end = [2018,2,28]

# get all the categories sold for a given time period
unique = commission.get_separated_sku(start, end)
categories = unique['categories']

# sum up the revenue
revenue = 0
for category in categories:
    revenue += commission.revenue_per_category(start, end, category = category)

print(revenue)