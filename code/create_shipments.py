from modules import shipengine
from modules import db, dude
from modules.db import Shipment, Weight
import requests, os

def add_undefined_boxes():
    ''' 
    Checks rows in the shipments table to ensure they have a weight and box. If they do not, a new entry containing the quantity per category sold
    is added to the weights table. The weights table tracks combinations purchased and assigns them a total order weight and box to be used for the order.
    


    If another customer purchases the same combination in the future, the shipment will not have to be manually weighed because the computer knows to 
    check if this combination has been weighed before. Not having to weigh each individual package drastically speeds up the packaging time. While this system 
    works quite well, it's already outdated. 


    All manual weighing should be eliminated in the future. The weight of each shipment should be automatically complicated by summing the products and shipping materials.
    It sounds simple but selecting the box is actually a fairly complicated problem to solve. 
    
    '''
    
    filter = Shipment.box == '0'
    objects = [Shipment.receipt_id]
    response = db.query(objects, filter).all()

    receipt_ids = []
    for row in response:
        receipt_id = row[0]
        receipt_ids.append(receipt_id)

        quantities = dude.quantity_per_category(receipt_id)['quantities']
        categories = dude.quantity_per_category(receipt_id)['categories']
        data = dict(zip(categories,quantities))
        object = Weight
        db.insert(object,data)

def update_undefined_boxes():

    filter = Weight.weight == 0
    objects = [Weight.EGR, Weight.EDU, Weight.EJA, Weight.ENI]
    response = db.query(objects, filter).all()

    for row in response:
        print(row)
        weight = input('Enter weight in ounces:')
        box = input('Enter box name:')
        updates = {'weight':weight, 'box':box}
        filter1 = Weight.EGR == row[0]
        filter2 = Weight.EDU == row[1]
        filter3 = Weight.EJA == row[2]
        filter4 = Weight.ENI == row[3] 
        filters = [filter1, filter2, filter3, filter4]

        db.update(Weight, updates, *filters)

def update_boxless_shipments():

    '''
    After undefined boxes have been updated in the weights table, this function updates the shipments
    '''


    # select the receipt_ids from the shipments table that have undefined boxes
    objects = [Shipment.receipt_id, Shipment.shipment_id]
    filter = Shipment.box == 'b433'
    response = db.query(objects,filter).all()

    for row in response:
        receipt_id = row[0]

        # recalculate & update the weight and box for a given receipt_id
        weight = dude.determine_total_weight(receipt_id)
        box = dude.determine_box(receipt_id)
        updates = {'weight':weight, 'box':box}
        filter = Shipment.receipt_id == receipt_id
        db.update(Shipment, updates, filter)


# using default values, create the shipments for all open orders
addresses = shipengine.get_open_addresses()
receipt_ids = shipengine.get_receipt_ids()
shipments = shipengine.create_default_shipments(addresses, receipt_ids)

# for any shipments that don't have a weight or box, update them
add_undefined_boxes()
update_undefined_boxes()
update_boxless_shipments()


# now that each shipment has a weight and box, send the shipments to shipengine 
shipment_ids = shipengine.send_shipments(shipments)
shipengine.add_shipment_ids_to_db(shipment_ids, receipt_ids)


# create a batch with all the shipment ids
batch = shipengine.create_batch(shipment_ids)
batch_id = batch['batch_id']
label_download = batch['label_download']


# process the batch
shipengine.process_batch(batch_id)




# create a webhook for when the batch has finished processing. Once that happens, send the zpl to the label printer

#once the hook has fired, query the labels and get tracking info and label cost for each shipment. Write both of these to the shipments table

def query_labels(batch_id):
    '''
    query the labels and get tracking information for each one 
    '''
    return None

# get the zpl & send it to the printer
zpl_labels = requests.get(label_download)





# get the receipt ids for shipments which don't have a box





















    
