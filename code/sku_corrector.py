# every transaction must have a sku for other parts of the program. Some of the etsy listing were missing them for a while so this applies corrections
# to our database. 

import collections
from modules import db
from modules. db import Transaction, Order



def get_unique_values(list_param):
    ''' Takes in a list and returns a list of all unique entries in that list'''

    # creates a dictionary with unique entires as keys and the count of each key as values  
    counter = collections.Counter(list_param)
    keys = counter.keys() # only interested in keys since the values aren't accurate due to quantities
    unique_list = list(keys)

    return unique_list

# define the time period
start_date = [2017,11, 1]
end_date = [2018, 3, 20]


# create the filters and get the data
objects = [Transaction.listing_id, Transaction.sku]
filter = ~Transaction.sku.like('E%') # where transaction.sku does not begin with E
relation = Order.receipt_id == Transaction.receipt_id
start_filter = db.start_filter(start_date)
end_filter = db.end_filter(end_date)
data = db.query(objects, relation, filter, start_filter, end_filter).all()


# get a list of unique listing ids that do not have a sku for the given time period 
listing_ids = []
for i in range(len(data)):
    listing_ids.append(data[i][0])

listing_ids = get_unique_values(listing_ids)


for listing_id in listing_ids:
    
    # get a list of all skus for each listing id
    objects = [Transaction.sku]
    filter = Transaction.listing_id == listing_id
    data = db.query(objects, relation, filter).all()

    # filter the skus such that we only have unique values
    skus = []
    for i in range(len(data)):
        skus.append(data[i][0])
    skus = get_unique_values(skus)

    # remove unique values which we don't want such as '' or '0'
    try:
        skus.remove('')
    except ValueError: #if the list doens't have '', this prevents a crash
        pass
    try:
        skus.remove('0')
    except ValueError:
        pass

    # sort the skus such that they're in alphabetical order
    skus = sorted(skus)
    print(skus)


    # And now for the grand finale: update rows in the db for a given listing_id & time period
    updates = {'sku':skus[0]}
    filter = Transaction.price == 29.99
    filter2 = Transaction.listing_id == listing_id
    updater = db.update(Transaction, updates, filter, filter2)

    # If there are mulitple skus for a listing id, do this
    if len(skus) > 1:
        updates = {'sku':skus[1]}
        filter = Transaction.price == 39.99
        db.update(Transaction, updates, filter, filter2)
   


