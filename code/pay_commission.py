import time
from modules import paypal, db
from modules.db import Commission


# set the time period
month = '02'
year = '2018'


# select artist_ids and commission amount for the given time period
objects = [Commission.artist_id,  Commission.commission]
filter1 = Commission.month == month
filter2 = Commission.year == year
data = db.query(objects, filter1, filter2).all()



# using those lists, generate the paypal payout items
items = []
for i in range(len(data)):
    artist_id = data[i][0]
    commission = data[i][1]
    item = paypal.create_payout_item(artist_id, commission, month, year)
    items.append(item)

# pay the commissions by making a request to paypal
response = paypal.pay(items, month, year)
print(response)
payout_batch_id = response[0]


# add payouts to the payout table 
time.sleep(15) # this should be replaced with a webhook
paypal.add_payouts_to_db(month, year, payout_batch_id)


