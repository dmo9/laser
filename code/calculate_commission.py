from modules import db, commission
from modules.db import Transaction, Commission, Order


''' 
ORGANIZE THE DATA
'''
# Define time period and create time filters
start_date = [2018, 2, 1]
end_date = [2018, 2, 28]
month = 2
year = 2018


commission.calculate_commission(month, year, start_date, end_date)
