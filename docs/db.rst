The database module is where database tables and the functions for interacting with the database are defined. 
Sqlalchemy is used as the ORM and postgres is used for the database. 



## creating database tables

Using sqlalchemy, we create a class that represents each table in the database. If a new class is added to this file and a script file calls this module, 
the new table will be created before the script is executed. 



## Interacting with the database 

Three functions allow us to interact with the database: query, insert, and update. The order of interacting with the db goes like this:

1. Start a new session using start_db_session
2. Instantiate an object and perform an action such as query. i.e session.query(), session.update()
3. If a filter has been passed as an arg, filter the instantiated object. This is equivalent to sql's where 
4. commit to the changes with session.commit()
5. Automatically close the session. This prevents from having too many open connections


## additional functions

There's a variety of filter functions which are used to store filters for specific queries. These are kinda old left over code that should
probably be eliminated. 


## future improvements

* date_to_epoch should be moved elsehere as its not a db specific function
* filtering functions could probably be eliminated 
* CRUD functions should be more consistent with one another 

