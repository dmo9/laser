
# Paying artists their commission

## overview
Paypal's payout api is one of the necessary ingredients in automating the process of calculating and paying artists their monthly commission.
Functions contained in that module interact with the paypal api as well as write payout information to the commissions table. The commissions module
contains functions which extract data for a given time period and calculate the commission for each artist.  



## order of operations:

1. All transaction data for a given month are extracted
2. From the skus, artist_ids are extracted
3. Revenue per commissioned artist is summed
4. commission is calculated based off revenue
5. commission amount for each artist is added to the commissions table. Only one entry per artist per month / year can be added
6. Funds are manually transferred from bank account to paypal balance
7. Once funds have cleared, payouts are sent to paypal via the paypal.py module
8. Each entry in the commissions table is updated with the payout fee, payout status, payout_item_id, and payout_batch_id     


## future improvements:
* a system of tests to ensure everything worked properly and artists were paid on time  


## modules used:
* paypal.py
* commission.py
* db.py

## scripts used:
*calculate_commisson.py
*pay_commission.py

